% include('head.tpl')

<div class="ui hidden divider"></div>
<div class="ui container">
<table  class="ui very basic table">
    <thead>
        <tr>

        <th>sid</th>
        <th>bname</th>
        <th>burl</th>
        <th>bmail</th>
        <th>bphone</th>
        <th>yelurl</th>
        <th>screenshoturl</th>
        <th>bssl</th>

        
        </tr>
    </thead>
    <tbody>
        %for r in r:
        <tr>


        <td>{{r.searchid}}</td>
        <td>{{r.bname}}</td>
        <td>{{r.burl}}</td>
        <td>{{r.bmail}}</td>
        <td>{{r.bphone}}</td>
        <td>{{r.yelpurl}}</td>
        <td><a href="https://screenshotapi.s3.amazonaws.com/captures/{{r.screenshoturl}}.png">{{r.screenshoturl}}</a></td>
        <td>{{r.bssl}}</td>
   

        </tr>
        %end
    </tbody>

</table>


</body>
</div>
</html>