import gevent
from gevent import monkey
monkey.patch_all()
import psycogreen.gevent
psycogreen.gevent.patch_psycopg()
import psycopg2
from peewee import *
import redis
import urllib3
from bs4 import BeautifulSoup as bs
import re
import certifi
import json
from model import *
from functools import lru_cache

db = PostgresqlDatabase('developerdb', user='developer', password='developer', autorollback=True)

r = redis.StrictRedis(host='localhost', port=6379, db=0)
viewer = r.pubsub(ignore_subscribe_messages=True)
viewer.subscribe('channelE')

head = json.dumps({"apikey":"dc92e2bb-2ffd-471b-80f6-a3d9b15dd2c0",
                      "Content-Type": "application/json"})
header = json.loads(head)
URL = 'https://api.screenshotapi.io/capture'



def capture(lead):
    burl, nanoid = lead
    siteurl = re.sub('((w)|(W))+\.', '', burl)
    body = json.dumps(dict(url=siteurl, webdriver='firefox', fresh='true', wait=1))
    http = (urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                                ca_certs=certifi.where()))
    gevent.sleep(5)
    resp = http.request('POST', URL, body=body, headers=header)
    resp_decoded = json.loads(resp.data.decode('utf-8', 'replace'))
    screenshot = tuple(resp_decoded.values())[1]
    print(screenshot)
    try:
        (Lead.update(screenshoturl=screenshot)
         .where(Lead.nanoid == nanoid)
         .execute())
    except Exception as err:
        return err


def get_burl_from_db(searchid):
    l = (Lead.select(Lead.nanoid, Lead.burl)
         .where(Lead.searchid == searchid)
         .filter(Lead.bssl == 'No')
         .execute())
    leads = [(l.burl, l.nanoid) for l in l]
    threads = [gevent.spawn(capture, lead) for lead in leads]
    gevent.joinall(threads)


def run_forever():
    while True:
        message = viewer.get_message()
        if not message:
            gevent.sleep(5)
        if message:
            try:
                m = json.loads(message['data'])['newsearch']
                newsearch = tuple(m.values())
                g = gevent.spawn(get_burl_from_db, newsearch[0])
                g.join()
            except Exception:
                pass

if __name__ == '__main__':
    run_forever()