from peewee import *
import pendulum


db = PostgresqlDatabase('developerdb', 
                        user='developer',
                        password='developer',
                        autorollback=True)


class BaseModel(Model):
    class Meta:
        database = db


class Search(BaseModel):
    searchid = AutoField(primary_key=True)
    userid = TextField()
    sterm = TextField()
    sloc = TextField()
    created = TimestampField(default=pendulum.now('UTC').int_timestamp)
    

class Lead(BaseModel):
    searchid = ForeignKeyField(Search, column_name='searchid')
    bname = TextField()
    burl = TextField(null=True)
    bphone = TextField(null=True)
    bmail = TextField(null=True)
    bssl = TextField(null=True)
    yelpurl = TextField()
    siteimage = TextField(null=True)
    nanoid = TextField()
    screenshoturl = TextField(null=True)
