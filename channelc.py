import gevent
from gevent import monkey
monkey.patch_all()
import psycogreen.gevent
psycogreen.gevent.patch_psycopg()
import psycopg2
import redis
from peewee import *
import urllib3
from urllib3.poolmanager import proxy_from_url
from model import *
import logging
import certifi
import json
from functools import lru_cache


r = redis.StrictRedis(host='localhost', port=6379, db=0)
viewer = r.pubsub(ignore_subscribe_messages=True)
viewer.subscribe('channelC')


@lru_cache(maxsize=256)
def check_ssl(lead):
    try:
        http = (urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                                    ca_certs=certifi.where()))
        url = 'https://' + lead[0]
        http.request('GET', url, timeout=10)
        bssl = 'Yes'
    except Exception as err:
        bssl = 'No'
    print('ssl', bssl)
    Lead.update(bssl=bssl).where(Lead.nanoid == lead[1]).execute()


def get_burl_from_db(searchid):
    l = (Lead.select(Lead.nanoid, Lead.burl)
         .where(Lead.searchid == searchid)
         .filter(Lead.burl != 'NOSITEURL')
         .execute())
    leads = [(l.burl, l.nanoid) for l in l]
    threads = [gevent.spawn(check_ssl, lead) for lead in leads]
    gevent.joinall(threads)
    message = {"newsearch": {"searchid": searchid}}
    m = json.dumps(message)
    r.publish('channelD', m)


def run_forever():
    while True:
        message = viewer.get_message()
        if not message:
            gevent.sleep(5)
        if message:
            try:
                m = json.loads(message['data'])['newsearch']
                newsearch = tuple(m.values())
                g = gevent.spawn(get_burl_from_db, newsearch[0])
                g.join()
            except Exception:
                pass


if __name__ == '__main__':
    run_forever()