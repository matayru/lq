import gevent
from gevent import subprocess
import os


BASE_PATH = os.path.abspath(os.path.curdir)
FILE = ['channela.py','channelb.py', 
    'channelc.py', 'channeld.py',
        'channele.py']
FILE_PATH = [os.path.join(BASE_PATH, f) for f in FILE]
VENV = '/home/moatairu/.local/share/virtualenvs/lq-cXevbrHN/bin/python'


def start_pipes():
    threads = [subprocess.Popen([VENV, f]) for f in FILE_PATH]
    


if __name__ == '__main__':
    g = gevent.spawn(start_pipes)
    gevent.wait()
