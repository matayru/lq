from bottle import template, Bottle, request, redirect
import redis
from peewee import *
from model import *
import uuid
import json

app = Bottle()
r = redis.StrictRedis()
r.pubsub()
proxies = ['35.206.70.39',
           '35.237.118.90',
           '35.230.66.232',
           '35.229.82.141',
           '35.228.189.87',
           '35.238.150.46',
           '35.231.231.52',
           '35.236.159.217',
           '35.231.222.69']


@app.route('/')
def home():
    return template('view.tpl')


@app.route('/user')
def result():
    result = (Search.select()
              .where(Search.userid == 'user@email.com')
              .execute())
    return template('user.tpl', r=result)


@app.route('/result/<sid:int>')
def result(sid):
    result = (Lead.select()
              .where(Lead.searchid == sid)
              .execute())
    return template('result.tpl', r=result)


@app.route('/search', method='GET')
def search():
    sloc = request.query.get('sloc')
    sterm = request.query.get('sterm')    
    proxy = request.query.get('proxy')
    sid = str(int(uuid.uuid4()))[:8]

    if proxy not in proxies:
        return f'{proxy} is not valid'

    (Search.insert({'userid': 'user@email.com',
                    'sterm': sterm,
                    'sloc': sloc,
                    'searchid': int(sid)})
                    .execute())
    message = {"newsearch": {"searchid": sid, 
                             "sterm": sterm, 
                             "sloc": sloc, 
                             "proxy": proxy}}
    m = json.dumps(message)
    r.publish('channelA', m)
    return redirect('/user')


app.run(host='localhost', port=8080)
