import gevent
from gevent import monkey
monkey.patch_all()
import psycogreen.gevent
psycogreen.gevent.patch_psycopg()
import psycopg2
from peewee import *
import redis
import urllib3
from bs4 import BeautifulSoup as bs
import re
import certifi
import json
from model import *
from urllib.parse import unquote
import pendulum
import logging

UA = ('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 ' +
      '(KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36')
EMAIL_REGEX = re.compile('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')
db = PostgresqlDatabase('developerdb', user='developer', password='developer', autorollback=True)

r = redis.StrictRedis(host='localhost', port=6379, db=0)
viewer = r.pubsub(ignore_subscribe_messages=True)
viewer.subscribe('channelD')


def find_email(lead):
        nanoid, burl = lead
        http = urllib3.PoolManager()
        nurl = re.sub('((w)|(W))+\.','', burl)
  
        try:
            hpage = http.request('GET', nurl, redirect=False, timeout=10).data.decode('utf-8', 'replace')
            bsoup = bs(hpage, 'html.parser')
        except Exception:
            Lead.update(bmail='NOEMAIL').where(Lead.nanoid == nanoid).execute() 
            return

        try:
            next_url = bsoup.find('a', href=re.compile('contact'))['href']
            if not next_url:
                next_url = nurl
            elif nurl not in next_url:
                next_url = nurl + '/' + next_url.replace('/', '')
        except Exception:
            Lead.update(bmail='NOEMAIL').where(Lead.nanoid == nanoid).execute() 
            return
                             
        try:
            bmail = re.search(EMAIL_REGEX, bsoup)[0]
        except Exception:
            cpage = http.request('GET', next_url, redirect=False, timeout=10).data.decode('utf-8', 'replace')
        try:
            bmail = re.search(EMAIL_REGEX, cpage)[0]
        except Exception:
            bmail = 'NOEMAIL'
        print(nurl, bmail)
        Lead.update(bmail=bmail).where(Lead.nanoid == nanoid).execute()


def get_burl_from_db(searchid):
    l = (Lead.select(Lead.nanoid, Lead.burl)
         .where(Lead.searchid == searchid)
         .filter(Lead.bssl == 'No')
         .execute())
    leads = [(l.nanoid, l.burl) for l in l]
    threads = [gevent.spawn(find_email, lead) for lead in leads]
    gevent.joinall(threads)
    message = {"newsearch": {"searchid": searchid}}
    m = json.dumps(message)
    r.publish('channelE', m)


def run_forever():
    while True:
        message = viewer.get_message()
        if not message:
            gevent.sleep(5)
        if message:
            try:
                m = json.loads(message['data'])['newsearch']
                newsearch = tuple(m.values())
                g = gevent.spawn(get_burl_from_db, newsearch[0])
                g.join()
            except Exception:
                pass


if __name__ == '__main__':
    run_forever()
