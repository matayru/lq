import gevent
from gevent import monkey
monkey.patch_all()
from psycogreen.gevent import patch_psycopg
patch_psycopg()
from peewee import *
import redis
from urllib3.poolmanager import proxy_from_url
from bs4 import BeautifulSoup as bs
import re
import certifi
import json
from model import *
from urllib.parse import unquote


r = redis.StrictRedis()
viewer = r.pubsub(ignore_subscribe_messages=True)
viewer.subscribe('channelB')


UA = ('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 ' +
      '(KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36')
URL_REGEX = re.compile("([A-Za-z0-9]+(-[A-Za-z0-9]+)*\.)+[A-Za-z]{2,}")


def get_site_url(lead):
    nanoid, yelpurl, proxy = lead
    try:
        http = proxy_from_url(f'http://{proxy}:80',
                              cert_reqs='CERT_REQUIRED', 
                              ca_certs=certifi.where())           
        response = http.request('GET', yelpurl, headers={'USER-AGENT': UA})

        # debug ?
        print(response.status)

        html = response.data.decode('utf-8', 'replace')
        bsoup = bs(html, 'html.parser')
        strain = bsoup.find('span', class_='biz-website').find('a')
        element = unquote(strain.get('href'))
        burl = re.search(URL_REGEX, element)[0]
    except BaseException as err:
        burl = 'NOSITEURL'
    try:
        (Lead.update(burl=burl)
         .where(Lead.nanoid == nanoid)
         .execute())
    except Exception as err:
        print(str(err))


def get_yelpurl_from_db(newsearch):
    searchid, proxy = newsearch
    l = (Lead.select(Lead.nanoid, Lead.yelpurl)
         .where(Lead.searchid == searchid)
         .execute())
    leads = [(l.nanoid, l.yelpurl, proxy) for l in l]
    threads = [gevent.spawn(get_site_url, lead) for lead in leads]
    gevent.joinall(threads)
    message = {"newsearch": {"searchid": searchid}}
    m = json.dumps(message)
    r.publish('channelC', m)


def run_forever():
    while True:
        message = viewer.get_message()
        print(message)
        if not message:
            gevent.sleep(5)
        if message:
            try:
                m = json.loads(message['data'])['newsearch']
                print(m)
                newsearch = tuple(m.values())
                g = gevent.spawn(get_yelpurl_from_db, newsearch)
                g.join()
            except Exception:
                pass


if __name__ == '__main__':
    run_forever()
